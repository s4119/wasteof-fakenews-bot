require('dotenv').config()
const axios = require('axios');
const token = process.env.TOKEN;


var textArray1 = [
  'Stock Photo Man',
  'No one',
  'A robot',
  'Triangle Player',
  'BrickPlanet Kid',
  'A cat',
  'This group of people'
];
var randomNumber1 = Math.floor(Math.random()*textArray1.length);
const character = textArray1[randomNumber1];

var textArray2 = [
  'running a mile!',
  'going to space!',
  'collecting songs!',
  'going to jail!',
  'making a wasteof.money account!',
  'monitoring media!',
  'clarifying a law!',
  'circling a fair!'
];
var randomNumber2 = Math.floor(Math.random()*textArray2.length);
const action1 = textArray2[randomNumber2];

const fakeEvent = 'this news is not real: ' + character + ' is ' + action1;


axios.get('https://api.wasteof.money/session', {
  headers: {
    'Authorization': `${token}`
  }
})
.then((res) => {
  console.log(res.data)
})
.catch((error) => {
  console.error(error)
})

const data = {
    post: `<p>${fakeEvent}</p>`
};

axios.post('https://api.wasteof.money/posts', data, {
    headers: {
        'Authorization': `${token}` 
      }
})
    .then((res) => {
        console.log(`Status: ${res.status}`);
        console.log('Body: ', res.data);
    }).catch((err) => {
        console.error(err);
    });


console.log(fakeEvent);