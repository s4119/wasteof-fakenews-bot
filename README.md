# wasteof-fakenews-bot

[my mastodon bot](https://techhub.social/@fakenewsbot) but it's on  https://wasteof.money/

## setup

this is made with `yarn`. to install yarn, run `npm i -g yarn`

install dependencies with the command `yarn`

copy `.env.example` to `.env`

add your bot username and password to .env. after that run `yarn run setup`.

once you do this, you should get a token. this allows you to send statuses as the bot.

copy this token into .env.

after the setup, simply run `yarn run start` to make a post. i recommend setting up a cron job for a certain time.
