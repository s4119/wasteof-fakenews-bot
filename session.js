require('dotenv').config()
const axios = require('axios');
const token = process.env.TOKEN;
const wasusername = process.env.USERNAME;
const waspassword = process.env.PASSWORD;


const data = {
  "username": `${wasusername}`,
  "password": `${waspassword}`  
};

axios.post('https://api.wasteof.money/session', data)
    .then((res) => {
        console.log(`Status: ${res.status}`);
        console.log('Body: ', res.data);
    }).catch((err) => {
        console.error(err);
    });

console.log('Copy the token returned and put it into .ENV');